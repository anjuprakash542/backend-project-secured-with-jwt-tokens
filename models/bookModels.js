const mongoose =require('mongoose');
const Author = require('./authorModel');
const bookSchema = new mongoose.Schema({
    bookName: String,
    image:String,
    author:String,
    description : String,
    authorId : {
      type:mongoose.ObjectId,
      ref:'Author'
    }
  });
  
  const Book = mongoose.model('Book', bookSchema);
  module.exports = Book
const bcrypt = require('bcrypt');
const User = require('../models/userModel');
const saltRounds = 10
const getAllUsers =  async (req,res) =>{
    try {
        const users = await User.find({}).exec();
        res.status(200).json(users)
    }
    catch (error) {
        res.status(500).send(error)
    }
}

const getUserById = async ( req, res) => {
    try {
    const user = await User.findById(req.params.userId).exec() 
    res.status(200).json(user)
    }
    catch (error) {
        res.status(404).send('userid not found')
    }
}

const addUser = async (req,res)=> {
    const userData = req.body;
    const hash = bcrypt.hashSync(userData.password,saltRounds);
    const user = new User({
        ...userData,
        password: hash
    })

    await user.save()
    res.status(201).json(user)
}

const updateUser = async (req,res) => {
     try {
        const user = await User.findByIdAndUpdate(req.params.userId , req.body, {new:true})
        res.json(user)
     } 

     catch (error) {
        res.status(404).send('error occured')
     }
}

const deleteUser = async(req,res) => {
    try {
        const user = await User.findByIdAndDelete (req.params.userId)
        res.send('deleted')
    }
   catch (error) {
        res.send(error)
   } 

}

module.exports = {
    getAllUsers, 
    getUserById,
    addUser, 
    updateUser,
    deleteUser
}
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken');
const User = require("../models/userModel")

const login = async (req,res) => {
    const {email,password} = req.body
    const user = await User.findOne({email:email})

    if(!user) {
        return(res.status(401).send('user with the provided email not found'))
    }

    const passwordMatch = bcrypt.compareSync(password, user.password)

    if(passwordMatch) {
        const token = jwt.sign ({id:user._id, email:user.email},'393090e5ec8da471c3ecec38ade72fc7c6baf3c00c153f1703e1e821d67a3103ea394a9ea2472e90304948877c4c17f5fd74e5eb90236c452636c7091f915798')
        res.cookie('token', token, {httpOnly:true})
        res.send('login success')
    }

    else {
        res.send('password doesnot match')
    }
}

const verifyLogin = async (req,res) => {
    if (req.cookies.token) {
      res.send('user logged in')
    }
     else {
      res.status(401).send('unauthorized access')
     }
  
  }
  
  
  
  module.exports={
      login,
      verifyLogin
  }